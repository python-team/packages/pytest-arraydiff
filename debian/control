Source: pytest-arraydiff
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-numpy,
               python3-pytest,
               python3-setuptools,
               python3-setuptools-scm
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/python-team/packages/pytest-arraydiff
Vcs-Git: https://salsa.debian.org/python-team/packages/pytest-arraydiff.git
Homepage: https://github.com/astropy/pytest-arraydiff

Package: python3-pytest-arraydiff
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Suggests: python3-astropy
Description: Pytest plugin to generate and compare data arrays
 The basic idea is that you can write a test that generates a Numpy array
 (or other related objects depending on the format). You can then either
 run the tests in a mode to generate reference files from the arrays, or
 you can run the tests in comparison mode, which will compare the results
 of the tests to the reference ones within some tolerance. At the moment,
 the supported file formats for the reference files are plaint text and
 FITS.
